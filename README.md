mergermarket-test
=================

MVP System to meet the requirements of docs/
 
## Summary ##

Thank you for the time taken to review this code.
The code and functionality has been kept simple, there are 
TODO markers in the code where I believe the code could be
adjusted to be more flexible, should the requirements change.

The code is using the symfony 3 framework. The framework is used to get things up and running
quickly and has only been setup to the base minimum, the code is all in the SRC dir all the tests are in TESTS dir

Using bitbucket over github as it allows free private repos, and using there CI pipelines

## Improvements if there was time ##
* docker and docker-compose files to run the application in a nginx / phpfpm environment
* add e2e tests
* add improvement listed in the code with TODO's
* add more to the phpdocs

## Prerequisites ##
* php >5.5
* ext-mongodb
* composer

## Install and build ##
* Clone from GIT
* `composer install`

Or if only running the application

* `composer install --no-dev`

## Run Tests ##
* `bin/phpcpd  src/`
* `bin/phpmd  src/  text cleancode,codesize,naming`
* `bin/phpcs --standard=PSR2 src/`
* `bin/phpunit --coverage-text` on php 5.*
* `phpdbg -qrr bin/phpunit --coverage-text` on php 7

Or run `./build.sh`

## Run Webserver (from the cli) ##
* `docker-compose up -d --build`
* Open server link in browser, normally http://127.0.0.1/