<?php
/**
 * Created by PhpStorm.
 * User: mattr
 * Date: 25/10/16
 * Time: 23:40
 */

namespace GMR\MMTestBundle\Service;

use GMR\MMTestBundle\Model\SentimentInterface;
use GMR\MMTestBundle\Model\Stock;
use GMR\MMTestBundle\Model\Story;

/**
 * Class FeedFetcher
 *
 * @package GMR\MMTestBundle\Service
 */
class FeedFetcher
{

    /** @var string  */
    protected $lookupUrl = 'http://mm-recruitment-stock-price-api.herokuapp.com/company/<<CODE>>';

    /**
     * @var SentimentInterface $sentiment
     */
    protected $sentiment;

    /**
     * Sentiment constructor.
     */
    public function __construct(SentimentInterface $sentiment, $lookupUrl = null)
    {
        $this->sentiment = $sentiment;
        if (!is_null($lookupUrl)) {
            $this->lookupUrl = $lookupUrl;
        }
    }

    public function getFeedURL($code)
    {
        return str_replace('<<CODE>>', $code, $this->lookupUrl);
    }

    /**
     * @param string $name
     * @param string $code
     * @param int    $storyLimit
     *
     * @return Stock
     * @throws FeedFetcherException
     */
    public function getStock($name, $code, $storyLimit = 10)
    {
        $feedUrl = $this->getFeedURL($code);

        $content = $this->getJSONContent($feedUrl);

        if (!is_array($content)) {
            throw new FeedFetcherException(FeedFetcherException::NOT_FOUND);
        }

        $stock = new Stock();
        $stock
            ->setName($name)
            ->setCode($content['tickerCode'])
            ->setLastUpdated(new \DateTimeImmutable($content['asOf']))
            ->setPrice($content['latestPrice'])
            ->setCurrency($content['priceUnits'])
        ;

        if (isset($content['storyFeedUrl'])) {
            $stock = $this->getStories($stock, $content['storyFeedUrl'], $storyLimit);
        }

        return $stock;
    }

    /**
     * @param Stock $stock
     * @param $storyFeedUrl
     * @return Stock
     */
    protected function getStories(Stock $stock, $storyFeedUrl, $storyLimit)
    {

        $stories = $this->getJSONContent($storyFeedUrl);

        if (!is_array($stories)) {
            return $stock;
        }

        $stories = array_reverse($stories);
        $count = 0;
        foreach ($stories as $story) {
            if (++$count > $storyLimit) {
                break;
            }

            $s = new Story();
            $s
                ->setBody($story['body'])
                ->setHeadline($story['headline'])
                ->setID($story['id'])
            ;

            $sentiment = $this->sentiment->analysis($s->getBody());
            $s->setSentiment($sentiment);
            $stock->addStory($s);
        }

        return $stock;
    }

    /**
     * Retrieve JSON content as an array
     *
     * This is a very simplistic version with no error handling
     * In a real world case this should have appropriate error handling,
     * Possibly cache both positive abd negatives for a short time
     * timeouts and abstraction
     *
     * @param $url
     *
     * @return array
     */
    private function getJSONContent($url)
    {
        $content = @file_get_contents($url);

        return json_decode($content, true);
    }
}
