<?php
/**
 * Created by PhpStorm.
 * User: mattr
 * Date: 25/10/16
 * Time: 21:50
 */

namespace GMR\MMTestBundle\Service;

use GMR\MMTestBundle\Model\SentimentInterface;

/**
 * Class Sentiment
 *
 *
 * ### Story sentiment analysis
 * Each story should be assigned a positivity score - its formula is:
 *
 * `positivity = positive_word_count - negative_word_count`
 *
 * - `positive_word_count` is the number of times a positive word appears in the article body
 * - `negative_word_count` is the number of times a negative word appears in the article body
 * - If the value of `positivity` is less than 2, the sentiment analysis is *neutral*.
 * - If `positivity` is positive, the sentiment analysis is *positive*.
 * - If `positivity` is negative, the sentiment analysis is *negative*.
 *
 * Positive words: positive, success, grow, gains, happy, healthy
 *
 * Negative words: disappointing, concerns, decline, drag, slump, feared
 *
 * Calculate each story's sentiment analysis
 *
 * @package GMR\MMTestBundle\Service
 */
class Sentiment implements SentimentInterface
{

    /**
     * Array to hold positive words
     *
     * @var string[] array
     */
    protected $positiveWords;

    /**
     * Array to hold negative words
     *
     * @var string[] array
     */
    protected $negativeWords;

    /**
     * Sentiment constructor.
     */
    public function __construct()
    {
        $this->positiveWords = ['positive', 'success', 'grow', 'gains', 'happy', 'healthy'];
        $this->negativeWords = ['disappointing', 'concerns', 'decline', 'drag', 'slump', 'feared'];
    }

    /**
     * {@inheritdoc}
     */
    public function analysis($text)
    {
        // Note using preg_match for time efficiency, for doing this ayanlisys there will be more performant ways
        $countPositive = preg_match_all('/(' . implode('|', $this->positiveWords) . ')/i', $text);
        $countNegative = preg_match_all('/(' . implode('|', $this->negativeWords) . ')/i', $text);

        $positivity = $countPositive - $countNegative;

        if ($positivity < 2) {
            return $positivity<0? self::SENTIMENT_NEGATIVE : self::SENTIMENT_NEUTRAL;
        }

        return self::SENTIMENT_POSITIVE;
    }
}
