<?php
/**
 * Created by PhpStorm.
 * User: mattr
 * Date: 26/10/16
 * Time: 14:29
 */

namespace GMR\MMTestBundle\Service;

class FeedFetcherException extends \Exception
{
    const NOT_FOUND = 404;
}
