<?php
/**
 * Created by PhpStorm.
 * User: mattr
 * Date: 09/11/16
 * Time: 15:09
 */

namespace GMR\MMTestBundle\Service;

class Auth
{
    private $cookieName;

    private $authEndpoint;

    private $authData;

    public function __construct($authEndpoint = null)
    {
        $this->cookieName = 'MERGERMARKET';

        $this->authEndpoint = 'http://127.0.0.1:8000/session/keep-alive/';
        if (!empty($authEndpoint)) {
            $this->authEndpoint = $authEndpoint;
        }
    }

    public function hasAuthInfo()
    {
        if (isset($_COOKIE[$this->cookieName])) {
            return true;
        }

        return false;
    }

    public function isValid($identifier = null)
    {
        if (!isset($identifier)) {
            $identifier = isset($_COOKIE[$this->cookieName])? $_COOKIE[$this->cookieName] : null;
        }

        if (empty($identifier)) {
            return false;
        }

        $content =$this->fetchData($this->authEndpoint . $identifier);

        $data = json_decode($content, true);

        if (!is_array($data) || !isset($data['userId'])) {
            return false;
        }

        $this->authData = $data;

        return true;
    }

    public function fetchData($url)
    {
        return file_get_contents($url);
    }

    public function getData()
    {
        return $this->authData;
    }

    public function isAuthenticated()
    {
        return isset($this->authData);
    }

    public function getAuthEndpoint()
    {
        return $this->authEndpoint;
    }
}
