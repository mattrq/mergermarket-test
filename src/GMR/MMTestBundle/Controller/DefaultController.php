<?php

namespace GMR\MMTestBundle\Controller;

use GMR\MMTestBundle\Document\Company;
use GMR\MMTestBundle\Service\FeedFetcher;
use GMR\MMTestBundle\Service\FeedFetcherException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class DefaultController
 *
 * @package GMR\MMTestBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {

        if (!$this->get('auth')->isValid()) {
            return new Response('<html><body><h1>Unauthorised</h1></body></html>', 401);
        }

        // TODO: Directly calling Doctrine Repository from controller is not
        // ideal, should be handled by additional layer
        /** @var Company[] $companies */
        $companies = $this->get('doctrine_mongodb')
            ->getRepository(Company::class)
            ->findAll();

        $data = [
            'cards' => []
        ];

        /** @var FeedFetcher $feedFetcher */
        $feedFetcher = $this->get('feed-fetcher');
        foreach ($companies as $company) {
            try {
                $stock = $feedFetcher->getStock($company->getName(), $company->getTickerCode(), 2);
                $data['cards'][] = $stock;
            } catch (FeedFetcherException $e) {
                // Ignore for now
            }
        }

        return $this->render('GMRMMTestBundle:Default:index.html.twig', $data);
    }
}
