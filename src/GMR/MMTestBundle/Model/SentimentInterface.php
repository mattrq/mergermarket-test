<?php

namespace GMR\MMTestBundle\Model;

/**
 * Interface SentimentInterface
 *
 * @package GMR\MMTestBundle\Model
 */
interface SentimentInterface
{

    const SENTIMENT_NEGATIVE = -1;
    const SENTIMENT_NEUTRAL  = 0;
    const SENTIMENT_POSITIVE = 1;

    /**
     * @param $text
     *
     * @return integer
     */
    public function analysis($text);
}
