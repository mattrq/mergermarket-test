<?php
/**
 * Created by PhpStorm.
 * User: mattr
 * Date: 26/10/16
 * Time: 00:05
 */

namespace GMR\MMTestBundle\Model;

/**
 * Class Story
 *
 * @package GMR\MMTestBundle\Model
 */
class Story
{

    /** @var  string */
    protected $identifier;
    /** @var  string */
    protected $body;
    /** @var  string */
    protected $headline;
    /** @var  int */
    protected $sentiment;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return Story
     */
    public function setId($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     *
     * @return Story
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @return string
     */
    public function getHeadline()
    {
        return $this->headline;
    }

    /**
     * @param string $headline
     *
     * @return Story
     */
    public function setHeadline($headline)
    {
        $this->headline = $headline;

        return $this;
    }

    /**
     * @return int
     */
    public function getSentiment()
    {
        return $this->sentiment;
    }

    /**
     * @param int $sentiment
     *
     * @return Story
     */
    public function setSentiment($sentiment)
    {
        $this->sentiment = $sentiment;

        return $this;
    }

    /**
     *
     * @return string
     */
    public function getFormattedSentiment()
    {
        switch ($this->sentiment) {
            case SentimentInterface::SENTIMENT_NEGATIVE:
                return 'negative';
            case SentimentInterface::SENTIMENT_POSITIVE:
                return 'positive';
            case SentimentInterface::SENTIMENT_NEUTRAL:
            default:
                return 'neutral';
        }
    }
}
