<?php
/**
 * Created by PhpStorm.
 * User: mattr
 * Date: 25/10/16
 * Time: 23:58
 */

namespace GMR\MMTestBundle\Model;

/**
 * Class Stock
 *
 * @package GMR\MMTestBundle\Model
 */
class Stock
{
    /** @var  string */
    protected $name;
    /** @var  string */
    protected $code;
    /** @var Story[] */
    protected $stories;
    /** @var \DateTime */
    protected $lastUpdated;
    /** @var  int */
    protected $price;
    /** @var string */
    protected $currency;

    /**
     * Stock constructor.
     */
    public function __construct()
    {
        $this->stories = [];
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     *
     * @return Stock
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     *
     * @return Stock
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Stock
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return Stock
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Story[]
     */
    public function getStories()
    {
        return $this->stories;
    }

    /**
     * @param Story $story
     *
     * @return Stock
     */
    public function addStory($story)
    {
        $this->stories[] = $story;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * @param \DateTime $lastUpdated
     *
     * @return Stock
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    /**
     * Return a formmated price
     *
     * Could use a decorator to present the information
     *
     * @return string
     * @throws \Exception
     */
    public function getFormattedPrice()
    {
        switch ($this->currency) {
            case 'GBP:pence':
                return $this->price . 'p';
            default:
                throw new \Exception('Unknown currency');
        }
    }
}
