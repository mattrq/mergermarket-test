<?php
/**
 * Created by PhpStorm.
 * User: mattr
 * Date: 26/10/16
 * Time: 13:51
 */

namespace GMR\MMTestBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="company")
 */
class Company
{
    /**
     * @MongoDB\Id
     */
    protected $identifier;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $name;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $tickerCode;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->identifier;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTickerCode()
    {
        return $this->tickerCode;
    }

    /**
     * @param mixed $tickerCode
     *
     * @return Company
     */
    public function setTickerCode($tickerCode)
    {
        $this->tickerCode = $tickerCode;

        return $this;
    }

    /**
     * @param mixed $identifier
     *
     * @return Company
     */
    public function setId($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }
}
