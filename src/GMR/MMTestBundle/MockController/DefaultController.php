<?php

namespace GMR\MMTestBundle\MockController;

use GMR\MMTestBundle\Document\Company;
use GMR\MMTestBundle\Service\FeedFetcher;
use GMR\MMTestBundle\Service\FeedFetcherException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class DefaultController
 *
 * @package GMR\MMTestBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * Mock route, if recived id is success return positive response
     * else negative
     * @Route("/session/keep-alive/{sessionId}")
     */
    public function indexAction($sessionId)
    {
        if (!empty($sessionId) && $sessionId=='success') {
            return $this->json(
                [
                    'userId'=>123,
                    'sessionToken' => '123123oi21',
                    'expiryDate' => (new \DateTime())->format('c'),
                    'rememberMe' => false
                ],
                200
            );
        }
        return $this->json(['message'=>'Unauthorised'], 401);
    }
}
