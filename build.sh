#!/bin/bash
#bin/phpcbf --standard=PSR2 --extensions=php src/
composer install
bin/phpcpd src/
bin/phpmd src/ text cleancode,codesize,naming
bin/phpcs --standard=PSR2 src
which phpdbg
if [ $? -eq 0 ]; then
    phpdbg -qrr bin/phpunit --coverage-text
else
    bin/phpunit --coverage-text
fi