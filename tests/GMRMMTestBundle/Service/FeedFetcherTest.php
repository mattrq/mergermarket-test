<?php
/**
 * Created by PhpStorm.
 * User: mattr
 * Date: 26/10/16
 * Time: 22:16
 */

namespace GMR\MMTestBundle\Test\Service;

use GMR\MMTestBundle\Model\SentimentInterface;
use GMR\MMTestBundle\Service\FeedFetcher;
use GMR\MMTestBundle\Service\FeedFetcherException;

class FeedFetcherTest extends \PHPUnit_Framework_TestCase
{
    public function testFeedURL()
    {
        $stub = $this->getMockBuilder(SentimentInterface::class)->getMock();
        $subject = new FeedFetcher($stub, '<<CODE>>');
        $this->assertEquals('TEST', $subject->getFeedURL('TEST'));
    }

    /**
     * @expectedException \GMR\MMTestBundle\Service\FeedFetcherException
     */
    public function testBadFeed()
    {
        $stub = $this->getMockBuilder(SentimentInterface::class)->getMock();
        $subject = new FeedFetcher($stub, 'file:///129836102938/<<CODE>>');
        $subject->getStock('Test', '213');
    }

    public function testGetStockNoStories()
    {
        $stub = $this->getMockBuilder(SentimentInterface::class)->getMock();
        $feedUrl = 'file://' . dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Fixture' . DIRECTORY_SEPARATOR . '<<CODE>>';
        $subject = new FeedFetcher($stub, $feedUrl);
        $subject->getStock('Test', 'NoStories');
    }

    public function testHasBadStories()
    {
        $pwd = getcwd();
        chdir(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Fixture');

        $stub = $this->getMockBuilder(SentimentInterface::class)->getMock();
        $feedUrl = 'file://' . dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Fixture' . DIRECTORY_SEPARATOR . '<<CODE>>';
        $subject = new FeedFetcher($stub, $feedUrl);
        $subject->getStock('Test', 'HasBadStories', 1);
        chdir($pwd);
    }

    public function testGetStockStories()
    {
        $pwd = getcwd();
        chdir(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Fixture');

        $stub = $this->getMockBuilder(SentimentInterface::class)->getMock();
        $feedUrl = 'file://' . dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Fixture' . DIRECTORY_SEPARATOR . '<<CODE>>';
        $subject = new FeedFetcher($stub, $feedUrl);
        $subject->getStock('Test', 'HasStories', 1);
        chdir($pwd);
    }

}
