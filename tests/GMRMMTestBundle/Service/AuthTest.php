<?php
/**
 * Created by PhpStorm.
 * User: mattr
 * Date: 26/10/16
 * Time: 22:16
 */

namespace GMR\MMTestBundle\Test\Service;

use GMR\MMTestBundle\Service\Auth;

class AuthTest extends \PHPUnit_Framework_TestCase
{

    public function testEndPointIsSet()
    {
       // $stub = $this->getMockBuilder(SentimentInterface::class)->getMock();
        $subject = new Auth('file:///129836102938/');
        $this->assertEquals('file:///129836102938/', $subject->getAuthEndpoint());
    }

    public function testEndPointIsPositive()
    {
        /** @var Auth $subject */
        $subject = $this->getMockBuilder(Auth::class)
            ->setMethods(['fetchData'])
            ->getMock();

        $subject->expects($this->once())
                ->method('fetchData')
                ->willReturn('{"userId": 123}');

        $this->assertEquals(true, $subject->isValid('121312'));
    }

    public function testEndPointIsPositiveWithCookie()
    {
        $_COOKIE['MERGERMARKET'] = 'success';
        /** @var Auth $subject */
        $subject = $this->getMockBuilder(Auth::class)
                        ->setMethods(['fetchData'])
                        ->getMock();

        $subject->expects($this->once())
                ->method('fetchData')
                ->willReturn('{"userId": 123}');

        $this->assertEquals(true, $subject->isValid());
    }

    public function testEndPointIsNegativeWithCookie()
    {
        $_COOKIE['MERGERMARKET'] = 'test';
        /** @var Auth $subject */
        $subject = $this->getMockBuilder(Auth::class)
                        ->setMethods(['fetchData'])
                        ->getMock();

        $subject->expects($this->once())
                ->method('fetchData')
                ->with($this->equalTo('http://127.0.0.1:8000/session/keep-alive/test'))
                ->willReturn('{}');

        $this->assertEquals(false, $subject->isValid());
        $this->assertEquals(false, $subject->isValid(''));
    }

    public function testDataIsEmpty()
    {
        $subject = new Auth();
        $this->assertEquals(null, $subject->getData());
    }

    public function testCheckIfCookieSet()
    {
        if (isset($_COOKIE['MERGERMARKET'])) {
            unset($_COOKIE['MERGERMARKET']);
        }
        $subject = new Auth();
        $this->assertEquals(false, $subject->hasAuthInfo());
        $_COOKIE['MERGERMARKET'] = 'foo';
        $this->assertEquals(true, $subject->hasAuthInfo());
    }

    public function testCheckIfAuthenticated()
    {
        $subject = new Auth();
        $this->assertEquals(false, $subject->isAuthenticated());
    }

    public function testCheckFetchData()
    {
        $feedUrl = 'file://' . dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Fixture' . DIRECTORY_SEPARATOR . '123';
        $subject = new Auth();
        $this->assertEquals('TEST', $subject->fetchData($feedUrl));
    }
//
//    public function testGetStockNoStories()
//    {
//        $stub = $this->getMockBuilder(SentimentInterface::class)->getMock();
//        $feedUrl = 'file://' . dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Fixture' . DIRECTORY_SEPARATOR . '<<CODE>>';
//        $subject = new FeedFetcher($stub, $feedUrl);
//        $subject->getStock('Test', 'NoStories');
//    }
//
//    public function testHasBadStories()
//    {
//        $pwd = getcwd();
//        chdir(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Fixture');
//
//        $stub = $this->getMockBuilder(SentimentInterface::class)->getMock();
//        $feedUrl = 'file://' . dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Fixture' . DIRECTORY_SEPARATOR . '<<CODE>>';
//        $subject = new FeedFetcher($stub, $feedUrl);
//        $subject->getStock('Test', 'HasBadStories', 1);
//        chdir($pwd);
//    }
//
//    public function testGetStockStories()
//    {
//        $pwd = getcwd();
//        chdir(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Fixture');
//
//        $stub = $this->getMockBuilder(SentimentInterface::class)->getMock();
//        $feedUrl = 'file://' . dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Fixture' . DIRECTORY_SEPARATOR . '<<CODE>>';
//        $subject = new FeedFetcher($stub, $feedUrl);
//        $subject->getStock('Test', 'HasStories', 1);
//        chdir($pwd);
//    }

}
