<?php
/**
 * Created by PhpStorm.
 * User: mattr
 * Date: 25/10/16
 * Time: 22:24
 */

namespace GMR\MMTestBundle\Tests\Service;

use GMR\MMTestBundle\Model\SentimentInterface;
use GMR\MMTestBundle\Service\Sentiment;

/**
 * Class SentimentTest
 *
 * @package GMR\MMTestBundle\Tests\Service
 */
class SentimentTest extends \PHPUnit_Framework_TestCase
{

    public function testPositiveSentiment()
    {
        $subject = new Sentiment();

        $this->assertEquals(SentimentInterface::SENTIMENT_POSITIVE, $subject->analysis('positive success'));

        $words = ['positive', 'success', 'grow', 'gains', 'happy', 'healthy'];
        foreach ($words as $word) {
            $this->assertEquals(SentimentInterface::SENTIMENT_POSITIVE, $subject->analysis('positive ' . $word));
        }

        $this->assertEquals(SentimentInterface::SENTIMENT_POSITIVE, $subject->analysis('positive ' . implode(' ', $words)));
    }

    public function testNeutralSentiment()
    {
        $subject = new Sentiment();

        $words = ['positive', 'success', 'grow', 'gains', 'happy', 'healthy'];
        foreach ($words as $word) {
            $this->assertEquals(SentimentInterface::SENTIMENT_NEUTRAL, $subject->analysis($word));
        }
    }

    public function testSimpleNegativeSentiment()
    {
        $subject = new Sentiment();

        $words = ['disappointing', 'concerns', 'decline', 'drag', 'slump', 'feared'];
        foreach ($words as $word) {
            $this->assertEquals(SentimentInterface::SENTIMENT_NEGATIVE, $subject->analysis($word));
        }

        $this->assertEquals(SentimentInterface::SENTIMENT_NEGATIVE, $subject->analysis('positive ' . implode(' ', $words)));
    }

    public function testEmptySentiment()
    {
        $subject = new Sentiment();

        $this->assertEquals(SentimentInterface::SENTIMENT_NEUTRAL, $subject->analysis(''));
    }

    public function testComplextSentiment()
    {
        $subject = new Sentiment();

        $this->assertEquals(SentimentInterface::SENTIMENT_NEGATIVE, $subject->analysis('disappointing positive concerns'));
        $this->assertEquals(SentimentInterface::SENTIMENT_NEUTRAL, $subject->analysis('healthy positive concerns'));
        $this->assertEquals(SentimentInterface::SENTIMENT_POSITIVE, $subject->analysis('success gains healthy positive concerns'));
    }
}
