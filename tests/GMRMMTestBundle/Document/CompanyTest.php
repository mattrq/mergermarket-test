<?php
/**
 * Created by PhpStorm.
 * User: mattr
 * Date: 26/10/16
 * Time: 20:46
 */

namespace GMR\MMTestBundle\Tests\Document;


use GMR\MMTestBundle\Document\Company;

/**
 * Class CompanyTest
 *
 * @package GMR\MMTestBundle\Tests\Document
 */
class CompanyTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Testing Setters
     */
    public function testSetters()
    {
        $subject = new Company();

        try {
            $this->assertEquals($subject, $subject->setId(1));
            $this->assertEquals($subject, $subject->setName('Test'));
            $this->assertEquals($subject, $subject->setTickerCode('GOOG'));
        } catch (\Exception $e) {
            $this->fail(" Exception has not been expected.");
        }
    }


    /**
     * Testing Getters
     */
    public function testGetters()
    {
        $subject = new Company();

        try {
            $this->assertEquals(null, $subject->getId());
            $this->assertEquals(null, $subject->getName());
            $this->assertEquals(null, $subject->getTickerCode());
        } catch (\Exception $e) {
            $this->fail(" Exception has not been expected.");
        }
    }

    public function testSetAndGet()
    {
        $subject = new Company();

        try {
            $this->assertEquals($subject, $subject->setId(1));
            $this->assertEquals($subject, $subject->setName('Test'));
            $this->assertEquals($subject, $subject->setTickerCode('GOOG'));
            $this->assertEquals(1, $subject->getId());
            $this->assertEquals('Test', $subject->getName());
            $this->assertEquals('GOOG', $subject->getTickerCode());
        } catch (\Exception $e) {
            $this->fail(" Exception has not been expected.");
        }
    }
}
