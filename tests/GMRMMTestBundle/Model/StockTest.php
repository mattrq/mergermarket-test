<?php
/**
 * Created by PhpStorm.
 * User: mattr
 * Date: 26/10/16
 * Time: 21:43
 */

namespace GMR\MMTestBundle\Test\Model;

use GMR\MMTestBundle\Model\Stock;
use GMR\MMTestBundle\Model\Story;

class StockTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Testing Setters
     */
    public function testSetters()
    {
        $subject = new Stock();

        try {
            $this->assertEquals($subject, $subject->setName('Test'));
            $this->assertEquals($subject, $subject->setLastUpdated(new \DateTimeImmutable()));
            $this->assertEquals($subject, $subject->setPrice(10));
            $this->assertEquals($subject, $subject->setCode('123'));
            $this->assertEquals($subject, $subject->setCurrency('GBP:pence'));
        } catch (\Exception $e) {
            $this->fail("Exception has not been expected.");
        }
    }

    /**
     * Testing Getters
     */
    public function testEmptyGetters()
    {
        $subject = new Stock();

        try {
            $this->assertEquals(null, $subject->getName());
            $this->assertEquals(null, $subject->getLastUpdated());
            $this->assertEquals(null, $subject->getPrice());
            $this->assertEquals(null, $subject->getCode());
            $this->assertEquals(null, $subject->getCurrency());
        } catch (\Exception $e) {
            $this->fail("Exception has not been expected.");
        }
    }

    public function testSetAndGet()
    {
        $subject = new Stock();

        try {
            $this->assertEquals($subject, $subject->setName('Test'));
            $this->assertEquals($subject, $subject->setLastUpdated($date = new \DateTimeImmutable()));
            $this->assertEquals($subject, $subject->setPrice(10));
            $this->assertEquals($subject, $subject->setCode('123'));
            $this->assertEquals($subject, $subject->setCurrency('GBP:pence'));
            $this->assertEquals('Test', $subject->getName());
            $this->assertEquals($date, $subject->getLastUpdated());
            $this->assertEquals(10, $subject->getPrice());
            $this->assertEquals('123', $subject->getCode());
            $this->assertEquals('GBP:pence', $subject->getCurrency());
        } catch (\Exception $e) {
            $this->fail("Exception has not been expected.");
        }
    }

    public function testFormatter()
    {
        try {
            $subject = new Stock();
            $subject
                ->setPrice(10)
                ->setCode('123')
                ->setCurrency('GBP:pence')
            ;
            $subject->getFormattedPrice();
        } catch (\Exception $e) {
            $this->fail("Exception has not been expected.");
        }

        try {
            $subject = new Stock();
            $subject->getFormattedPrice();
            $this->fail("Exception was expected.");
        } catch (\Exception $e) {
            $this->assertEquals('Unknown currency', $e->getMessage());
        }
    }

    public function testStories()
    {
        $stub = $this->getMockBuilder(Story::class)->getMock();

        $subject = new Stock();
        $this->assertEquals($subject, $subject->addStory($stub));

        $subject = new Stock();
        $this->assertEquals([], $subject->getStories());

        $subject = new Stock();
        $subject->addStory($stub);
        $this->assertEquals([$stub], $subject->getStories());
    }
}
