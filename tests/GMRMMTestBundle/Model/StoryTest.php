<?php
/**
 * Created by PhpStorm.
 * User: mattr
 * Date: 26/10/16
 * Time: 21:43
 */

namespace GMR\MMTestBundle\Test\Model;

use GMR\MMTestBundle\Model\SentimentInterface;
use GMR\MMTestBundle\Model\Stock;
use GMR\MMTestBundle\Model\Story;

class StoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Testing Setters
     */
    public function testSetters()
    {
        $subject = new Story();

        try {
            $this->assertEquals($subject, $subject->setID('123'));
            $this->assertEquals($subject, $subject->setBody('Test'));
            $this->assertEquals($subject, $subject->setHeadline('Test'));
            $this->assertEquals($subject, $subject->setSentiment(SentimentInterface::SENTIMENT_NEUTRAL));
        } catch (\Exception $e) {
            $this->fail("Exception has not been expected.");
        }
    }

    /**
     * Testing Getters
     */
    public function testEmptyGetters()
    {
        $subject = new Story();

        try {
            $this->assertEquals(null, $subject->getID());
            $this->assertEquals(null, $subject->getBody());
            $this->assertEquals(null, $subject->getHeadline());
            $this->assertEquals(null, $subject->getSentiment());
        } catch (\Exception $e) {
            $this->fail("Exception has not been expected.");
        }
    }

    public function testSetAndGet()
    {
        $subject = new Story();

        try {
            $this->assertEquals($subject, $subject->setID('123'));
            $this->assertEquals($subject, $subject->setBody('Test'));
            $this->assertEquals($subject, $subject->setHeadline('Test'));
            $this->assertEquals($subject, $subject->setSentiment(SentimentInterface::SENTIMENT_NEUTRAL));
            $this->assertEquals('123', $subject->getID());
            $this->assertEquals('Test', $subject->getBody());
            $this->assertEquals('Test', $subject->getHeadline());
            $this->assertEquals(SentimentInterface::SENTIMENT_NEUTRAL, $subject->getSentiment());
        } catch (\Exception $e) {
            $this->fail("Exception has not been expected.");
        }
    }

    public function testEmptySentimentFormatter()
    {
        $subject = new Story();
        $this->assertEquals('neutral', $subject->getFormattedSentiment());
    }

    public function testNeutralSentimentFormatter()
    {
        $subject = new Story();
        $subject->setSentiment(SentimentInterface::SENTIMENT_NEUTRAL);
        $this->assertEquals('neutral', $subject->getFormattedSentiment());
    }

    public function testNegativeSentimentFormatter()
    {
        $subject = new Story();
        $subject->setSentiment(SentimentInterface::SENTIMENT_NEGATIVE);
        $this->assertEquals('negative', $subject->getFormattedSentiment());
    }

    public function testPositiveSentimentFormatter()
    {
        $subject = new Story();
        $subject->setSentiment(SentimentInterface::SENTIMENT_POSITIVE);
        $this->assertEquals('positive', $subject->getFormattedSentiment());
    }

}
