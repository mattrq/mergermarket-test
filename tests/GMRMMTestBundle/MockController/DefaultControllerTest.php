<?php

namespace GMR\MMTestBundle\Tests\MockController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    /**
     * @group integrationTest
     */
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertContains('Mergermarket', $client->getResponse()->getContent());
    }
}
